package com.yc.utils;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.yc.midd.interpreter.CEnv;

public class YCLogger {

	
	static public String log(Level level, String message)
	{
		if (CEnv.isLogging())
		{
			final int track = 2;
			Logger.getLogger(
					Thread.currentThread().getStackTrace()[track].getClassName().concat(".").concat(
							Thread.currentThread().getStackTrace()[track].getMethodName()).concat("()")).log(level, message);
		}
		
		return message;
	}
	
	static public String log(String message)
	{
		if (CEnv.isLogging())
		{
			final int track = 2;
			Logger.getLogger(
					Thread.currentThread().getStackTrace()[track].getClassName().concat(".").concat(
							Thread.currentThread().getStackTrace()[track].getMethodName()).concat("()")).info(message);
		}
		
		return message;
	}
	
	static public String log(Level level, Exception e)
	{
		final String exceptionMessage = e.getMessage().concat(e.getCause() == null ? "" : e.getCause().getMessage());
		
		if (CEnv.isLogging())
		{
			final int track = 2;
			Logger.getLogger(
					Thread.currentThread().getStackTrace()[track].getClassName().concat(".").concat(
							Thread.currentThread().getStackTrace()[track].getMethodName()).concat("()"))
									.log(level, exceptionMessage);
			e.printStackTrace();
		}
		
		return exceptionMessage;
	}
	
	static public String log(Exception e)
	{
		e.printStackTrace();
		final String exceptionMessage = e.getMessage().concat(e.getCause() == null ? "" : e.getCause().getMessage());
		
		if (CEnv.isLogging())
		{
			final int track = 2;
			Logger.getLogger(
					Thread.currentThread().getStackTrace()[track].getClassName().concat(".").concat(
							Thread.currentThread().getStackTrace()[track].getMethodName()).concat("()")).info(exceptionMessage);
			e.printStackTrace();
		}
		
		return exceptionMessage;
	}
}
