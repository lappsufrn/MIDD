package com.yc.utils;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.logging.Logger;

import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.fasterxml.jackson.core.io.JsonStringEncoder;

public class ErrorCatcher
{
	private static final Logger log = Logger.getLogger(ErrorCatcher.class.getName());

	public ErrorCatcher ()
	{
		
	}
	
	public ResponseEntity<String> exceptionCatcher (Exception e, HttpStatus httpStatus)
	{
		final JSONObject exception = new JSONObject();
		
		try
		{
			final StringWriter errors = new StringWriter();
			
			e.printStackTrace(new PrintWriter(errors));
			
			exception.put(Constant.data, new JSONObject());
			exception.getJSONObject(Constant.data).put(Constant.exception, e.getClass().getSimpleName());
			exception.getJSONObject(Constant.data).put(Constant.message, e.getMessage());
			exception.getJSONObject(Constant.data).put(Constant.stackTrace, errors.toString());
			
			if (e.getCause() != null)
			{
				exception.getJSONObject(Constant.data).put(Constant.cause, 
						String.valueOf(JsonStringEncoder.getInstance().quoteAsString(e.getCause().toString())));
			}
			else
			{
				exception.getJSONObject(Constant.data).put(Constant.cause, "");
			}
			
			log.info("EXCEPTION: GRAVE: ".concat(exception.getJSONObject(Constant.data).getString(Constant.stackTrace)));
		}
		catch (Exception ex)
		{
			log.info("EXCEPTION: CRITICAL: ".concat(ex.getMessage()));
			
			ex.printStackTrace();
		}
		
		return new ResponseEntity<String>(exception.toString(), httpStatus);
	}
	
	public String methodCanonicalName(int level)
	{
		final String methodName = Thread.currentThread().getStackTrace()[level].getMethodName();
		return Thread.currentThread().getStackTrace()[level]
				.getClassName().concat(".").concat(methodName).concat("()");
	}
	
	public static void main(String [] args)
	{
		
	}
}
