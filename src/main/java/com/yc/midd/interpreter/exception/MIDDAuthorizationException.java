package com.yc.midd.interpreter.exception;

@SuppressWarnings("serial")
public class MIDDAuthorizationException extends Exception
{
	public MIDDAuthorizationException(String message)
	{
		super(message);
	}
}
