package com.yc.midd.interpreter.config;

import org.apache.commons.io.IOUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.yc.midd.interpreter.CEnv;
import com.yc.utils.YCLogger;

@RequestMapping("/private")
@RestController()
public class ModelUpload
{
	@Autowired
	private Environment environment;
	
	@PostMapping(path = "/upload/model", produces = MediaType.TEXT_PLAIN_VALUE)
	public ResponseEntity<String> uploadToLocalFileSystem(@RequestParam("file") MultipartFile file) throws JSONException 
	{
		String message = null;
		
		if (Boolean.valueOf(this.environment.getProperty("midd.onboot"))) 
		{
			try
			{
				String model = IOUtils.toString(file.getInputStream(), "UTF-8");
				
				CEnv.getInstance().buildSQLStatements(new JSONObject(model));
				
				YCLogger.log("SQL Statemens installed ... OK!");
				
				message = "Model installed!";
			} 
			catch (JSONException e)
			{
				e.printStackTrace();
				
				message = e.getMessage();
			} 
			catch (Exception e)
			{
				e.printStackTrace();
				
				message = e.getMessage();
			}
		}
		
		return ResponseEntity.ok(message);
	}
}