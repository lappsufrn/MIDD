package com.yc.midd.interpreter.service;

import org.json.JSONObject;

import com.yc.midd.interpreter.exception.MIDDControlledException;

public interface PersistenceService
{
	public String createOne(JSONObject jsonObject) throws MIDDControlledException, Exception;
	
	public String readById(String classUID, Long id) throws MIDDControlledException, Exception;
	
	public String readByCriteria(JSONObject jsonObject) throws MIDDControlledException, Exception;
	
	public String updateOne(JSONObject jsonObject) throws MIDDControlledException, Exception;
	
	public void deleteOne(String classUID, long id) throws MIDDControlledException, Exception;
}
