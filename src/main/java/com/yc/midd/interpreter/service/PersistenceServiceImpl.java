package com.yc.midd.interpreter.service;

import javax.annotation.Resource;

import org.json.JSONObject;
import org.springframework.stereotype.Service;

import com.yc.midd.interpreter.exception.MIDDControlledException;
import com.yc.midd.interpreter.repository.CreateDSH;
import com.yc.midd.interpreter.repository.DeleteDSH;
import com.yc.midd.interpreter.repository.ReadDSH;
import com.yc.midd.interpreter.repository.UpdateDSH;

@Service
public class PersistenceServiceImpl implements PersistenceService 
{
	@Resource
	public CreateDSH createDSH;
	
	@Resource
	public ReadDSH readDSH;
	
	@Resource
	public UpdateDSH updateDSH;
	
	@Resource
	public DeleteDSH deleteDSH;
	
	@Override
	public String createOne(JSONObject jsonObject) throws MIDDControlledException, Exception
	{
		return this.createDSH.saveOne(jsonObject).toString();
	}
	
	@Override
	public String readById(String classUID, Long id) throws MIDDControlledException, Exception
	{
		return this.readDSH.findById(classUID, id).toString();
	}
	
	@Override
	public String readByCriteria(JSONObject jsonObject) throws MIDDControlledException, Exception
	{
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public String updateOne(JSONObject jsonObject) throws MIDDControlledException, Exception
	{
		return this.updateDSH.updateOne(jsonObject).toString();
	}
	
	@Override
	public void deleteOne(String classUID, long id) throws MIDDControlledException, Exception
	{
		this.deleteDSH.deleteOne(classUID, id);
	}
}
