package com.yc.midd.interpreter.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.yc.midd.interpreter.CEnv;
import com.yc.midd.interpreter.bsttm.SQLStatements;
import com.yc.midd.interpreter.bsttm.Type;
import com.yc.utils.Constant;

@Repository
public class UpdateDSHImpl extends DSHImpl implements UpdateDSH
{
	@Autowired
	private DataSource dataSource;
	
	@Override
	public JSONObject updateOne(JSONObject jsonObject) 
			throws Exception
	{
		final JSONObject model = CEnv.getDataModel();
		
		try (final Connection connection = this.dataSource.getConnection();)
		{
			try
			{
				UpdateDSHImpl.this.operation(jsonObject, model, connection);
				
				connection.commit();
			}
			catch (SQLException e)
			{
				connection.rollback(); 
				
				if (Constant.erroCode.equals(e.getSQLState())) 
				{
					throw new SQLException(Constant.httpErrorCode.concat(e.getMessage()));
				}
				
				throw e;
			}
		}
		
		return jsonObject;
	}
	
	protected void operation(JSONObject object, JSONObject model, Connection connection) throws Exception
	{
		String classUID = String.valueOf(object.remove(Constant.classUID));
		Long id = Long.parseLong(object.remove(Constant.id).toString());
		
		try (PreparedStatement preparedStatement = 
				connection.prepareStatement(SQLStatements.getStatements()
						.getJSONObject(classUID).getString(SQLStatements.UPDATE_)
						.concat(String.valueOf(id)));)
		{
			JSONObject objectSpec = this.getSpec(classUID, object, model, connection);
			
			objectSpec.keySet().parallelStream().forEach(attributeNameObjectSpec -> 
			{
				try
				{
					JSONObject attributeSpec = 
							objectSpec.getJSONObject(attributeNameObjectSpec);
					if (attributeNameObjectSpec.equals(Constant.id))
					{
						
					}
					else if (attributeNameObjectSpec.equals(Constant._classDef))
					{
						
					}
					else if (object.has(attributeNameObjectSpec)) 
					{
						if (model.has(attributeSpec.getString(Constant.type))) 
						{
							JSONObject associatedObject = 
									object.getJSONObject(attributeNameObjectSpec);
							if (associatedObject.isNull(Constant.id))
							{
								UpdateDSHImpl.this.operation(associatedObject, model, connection);
							}
							
							preparedStatement.setObject(
									attributeSpec.getInt(Constant.sqlIndex), 
									associatedObject.get(Constant.id), 
									Type.SQL.Code.BIGINT);
						}
						else
						{
							preparedStatement.setObject(
									attributeSpec.getInt(Constant.sqlIndex), 
									object.get(attributeNameObjectSpec), 
									attributeSpec.getInt(Constant.sqlType));
						}
					}
					else 
					{
						preparedStatement.setObject(
								attributeSpec.getInt(Constant.sqlIndex), 
								null, Type.SQL.Code.NULL);
					}
				}
				catch (JSONException | SQLException e)
				{
					throw new RuntimeException(e.getMessage());
				}
				catch (Exception e)
				{
					throw new RuntimeException(e.getMessage());
				}
			});
			
			preparedStatement.execute();
			
			object.put("id", id);
			object.put(Constant.classUID, classUID);
		}
		catch (Exception e)
		{
			throw e;
		}
	}
	
	@Override
	public JSONArray update(JSONObject jsonObject) throws Exception
	{
		
		return null;
	}
}
