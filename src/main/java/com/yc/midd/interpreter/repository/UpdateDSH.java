package com.yc.midd.interpreter.repository;

import org.json.JSONArray;
import org.json.JSONObject;

public interface UpdateDSH
{
	public JSONArray update(JSONObject jsonObject) throws Exception;
	
	public JSONObject updateOne(JSONObject jsonObject) throws Exception;
}
