package com.yc.midd.interpreter.repository;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.sql.DataSource;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.yc.midd.interpreter.CEnv;
import com.yc.midd.interpreter.bsttm.SQLStatements;
import com.yc.midd.interpreter.repository.emap.RowMapper;
import com.yc.utils.Constant;

@Repository
public class ReadDSHImpl implements ReadDSH
{
	@Autowired
	private DataSource dataSource;
	
//	private ReadDSHImplUtils implUtils;
	
	public ReadDSHImpl()
	{
//		this.implUtils = new ReadDSHImplUtils();
	}

	@Override
	public JSONObject findById(final String classUID, Long id) 
			throws Exception
	{
		JSONObject response = null;
		final String sqlQuery = SQLStatements.getStatements()
				.getJSONObject(classUID)
				.getJSONObject(SQLStatements.SELECT)
				.getString(SQLStatements.Zero.toString())
				.concat(Constant.where_id).concat(id.toString());
		
		try (Connection connection = dataSource.getConnection();)
		{
			try (Statement statement = connection.createStatement();)
			{
				try (ResultSet resultSet = statement.executeQuery(sqlQuery);)
				{
					response = new RowMapper().getData(classUID, null, null, 
							CEnv.getDataModel(), resultSet);
					response.put(Constant.classUID, classUID);
				}
				catch (Exception e)
				{
					throw e;
				}
			}
			catch (Exception e)
			{
				throw e;
			}
		}
		catch (Exception e)
		{
			throw e;
		}
		
		return response;
	}

	@Override
	public JSONObject findByCriteria(JSONObject jsonObject) throws Exception
	{
		// TODO Auto-generated method stub
		return null;
	}
	
//	@Override
//	public JSONObject find(JSONObject jsonObject) throws Exception
//	{
//		final JSONObject model = CEnv.getDataModel();
//		
////		final String group = jsonObject.getString(Constant.group);
//		final JSONObject object = jsonObject.getJSONObject(Constant.object);
//		final JSONObject criterion = jsonObject.getJSONObject(Constant.criterion);
//		final JSONObject associations = jsonObject.getJSONObject(Constant.associations);
//		
//		final String classUID = object.getString(Constant.classUID);
//
//		final JSONObject response = new JSONObject();
//		
//		Connection connection = null;
//		Statement statement = null;
//		ResultSet resultSet = null;
//		
//		try
//		{
//			final StringBuffer filters = new StringBuffer();
//			
//			this.configQuery(object, null, null, criterion, filters, 
//					associations.getInt(Constant.level), 
//					associations.getBoolean(Constant.mode), 
//					new HashMap<String, Integer>(), model);
//			
//			if (filters.length() > 0)
//			{
//				if (criterion.getString(Constant.connective).trim().toUpperCase()
//						.equals("AND"))
//				{
//					filters.delete(filters.length() - 5, filters.length());
//				}
//				else
//				{
//					filters.delete(filters.length() - 4, filters.length());
//				}
//			}
//			
//			String sqlQuery = null;
//			
//			final String level;
//			final String toCountLevel;
//			if (associations.getInt(Constant.level) == 2)
//			{
//				level = SQLStatements.Two.toString();
//				toCountLevel = SQLStatements.toCountTwo.toString();
//			}
//			else if (associations.getInt(Constant.level) == 1)
//			{
//				level = SQLStatements.One.toString();
//				toCountLevel = SQLStatements.toCountOne.toString();
//			}
//			else
//			{
//				level = SQLStatements.Zero.toString();
//				toCountLevel = SQLStatements.toCountZero.toString();
//			}
//			
//			connection = dataSource.getConnection();
//			statement = connection.createStatement();
//			
//			if (criterion.getBoolean(Constant.toCount))
//			{
//				if (filters.length() > 0)
//				{
//					sqlQuery = SQLStatements.getStatements().getJSONObject(classUID)
//							.getJSONObject(SQLStatements.SELECT).getString(toCountLevel)
//							.concat(Constants.where).concat(filters.toString());
//					resultSet = statement.executeQuery(sqlQuery);
//				}
//				else
//				{
//					sqlQuery = SQLStatements.getStatements()
//							.getJSONObject(classUID)
//							.getJSONObject(SQLStatements.SELECT)
//							.getString(toCountLevel);
//					resultSet = statement.executeQuery(sqlQuery);
//				}
//				
//				resultSet.next();
//				response.put(Constant.totalRegisters, resultSet.getInt(1));
//				resultSet.close();
//				
//				statement.clearBatch();
//			}
//			
//			final String paging = " ORDER BY ".concat(classUID.toLowerCase())
//					.concat("_0.")
//					.concat(criterion.getString("orderBy")).concat(" ").concat(criterion.getString("order"))
//					.concat(" LIMIT ").concat(String.valueOf(criterion.getInt("maxRegisters")))
//					.concat(" OFFSET ").concat(String.valueOf(criterion.getInt("firstRegister")));
//			
//			if (filters.length() > 0)
//			{
//				sqlQuery = SQLStatements.getStatements()
//						.getJSONObject(classUID)
//						.getJSONObject(SQLStatements.SELECT)
//						.getString(level).concat(" WHERE ").concat(filters.toString()).concat(paging);
//			}
//			else
//			{
//				sqlQuery = SQLStatements.getStatements()
//						.getJSONObject(classUID)
//						.getJSONObject(SQLStatements.SELECT)
//						.getString(level).concat(paging);
//			}
//			
//			// CEnv.log("SQL Query: ".concat(sqlQuery));
//			
//			resultSet = statement.executeQuery(sqlQuery);
//			response.put(Constant.data, 
//					new RowMapper().getData(classUID, null, null, 
//							associations.getInt(Constant.level), 
//							associations.getBoolean(Constant.mode), model, resultSet));
//		}
//		finally
//		{
//			if (connection != null && !connection.isClosed()) 
//			{
//				connection.commit();
//				if (statement != null && !statement.isClosed()) 
//				{
//					statement.close();
//				}
//				connection.close();
//			}
//		}
//		
//		return response;
//	}
	
//	private void configQuery (final JSONObject object, 
//			String auxClassUID, 
//			final String specializedClazzName,
//			final JSONObject criterion,
//			final StringBuffer filters, 
//			Integer hierarchyLevel, 
//			final Boolean associationMode, 
//			final Map<String, Integer> associationsClazzes,
//			final JSONObject model) throws Exception
//	{
//		final String classUID = object.remove(Constant.classUID).toString().trim();
//		final String lcClassUID = classUID.toLowerCase();
//		Boolean isHierarchyTop = false;
//		
//		if (auxClassUID == null) auxClassUID = classUID.trim();
//		
//		if (model.getJSONObject(classUID).getJSONObject(Constant._classDef)
//				.has(Constant._extends))
//		{
//			this.implUtils.defineAlias(classUID, model, associationsClazzes);
//		}
//		else 
//		{
//			if (specializedClazzName != null) 
//			{
//				isHierarchyTop = true;
//			}
//			
//			if (!associationsClazzes.containsKey(lcClassUID)) 
//			{
//				associationsClazzes.put(lcClassUID, 0);
//			}
//		}
//		
//		final JSONObject objectSpec = model.getJSONObject(classUID);
//
//		final Iterator<String> iterator = object.keys();
//		while (iterator.hasNext())
//		{
//			final String attributeObjectName = iterator.next().trim();
//			final String lcAttributeSpecName = attributeObjectName.toLowerCase();
//			if (objectSpec.has(attributeObjectName))
//			{
//				final JSONObject attributeSpec = objectSpec.getJSONObject(attributeObjectName);
//				if (model.has(attributeSpec.getString(Constant.type)))
//				{
//					if (associationMode && hierarchyLevel> 0)
//					{
//						hierarchyLevel--;
//						this.configQuery(object.getJSONObject(attributeObjectName), attributeObjectName, null, criterion, 
//								filters, hierarchyLevel, associationMode, associationsClazzes, model);
//						hierarchyLevel++;
//					}
//					else if (object.getJSONObject(attributeObjectName).has(Constant.id)  
//							&& object.getJSONObject(attributeObjectName).getLong(Constant.id) > 0)
//					{
//						final String param = lcClassUID
//								.concat("_").concat(associationsClazzes.get(lcClassUID).toString())
//								.concat(".")
//								.concat(lcAttributeSpecName);
//						final Long value = object.getJSONObject(attributeObjectName).getLong(Constant.id);
//						
//						filters.append(param).append(" = ").append(value.toString())
//								.append(" ").append(criterion.getString(Constant.connective).trim().toUpperCase()).append(" ");
//					}
//				}
//				else
//				{
//					final StringBuffer operator = new StringBuffer();
//					String connective = " AND ";
//					String value = null;
//					Integer flag = 0;
//					String supValue = null;
//					
//					if (object.get(attributeObjectName) instanceof JSONObject) 
//					{
//						final JSONObject desc = object.getJSONObject(attributeObjectName);
//						if (desc.getString(Constant.type).equals(Constant.limit))
//						{
//							operator.delete(0, operator.length());
//							if (desc.getJSONObject(Constant.limit).getString(Constant.mode).equalsIgnoreCase(Constant.major)) 
//							{
//								operator.append(" >");
//							}
//							else
//							{
//								operator.append(" <");
//							}
//							
//							if (desc.getJSONObject(Constant.limit).getBoolean(Constant.closed)) 
//							{
//								operator.append("= ");
//							}
//							else
//							{
//								operator.append(" ");
//							}
//							
//							value = Type.getInstance().getValueFormattedByType(desc.getJSONObject(Constant.limit)
//									.get(Constant.value), attributeSpec.getString(Constant.type));
//						}
//						else if (desc.getString(Constant.type).equals(Constant.range))
//						{
//							operator.delete(0, operator.length());
//							if (desc.getJSONObject(Constant.range).getBoolean(Constant.closed)) 
//							{
//								flag = 2;
//								operator.append(" >= ");
//							}
//							else
//							{
//								flag = 1;
//								operator.append(" > ");
//							}
//							
//							value = desc.getJSONObject(Constant.range).getString(Constant.inf);
//							supValue = desc.getJSONObject(Constant.range).getString(Constant.sup);
//							connective = desc.getJSONObject(Constant.range).getString(Constant.connective);
//						}
//						else if (desc.getString(Constant.type).equals(Constant.in)) 
//						{
//							operator.delete(0, operator.length());
//							operator.append(" IN ");
//							
//							final JSONArray values = desc.getJSONArray(Constant.in);
//							StringBuffer buffer = new StringBuffer("(");
//							for (int index = 0; index < values.length(); index++)
//							{
//								buffer.append(Type.getInstance().getValueFormattedByType(
//										values.get(index), attributeSpec.getString(Constant.type))).append(", ");
//							}
//							buffer.delete(buffer.length() - 2, buffer.length());
//							value = buffer.append(")").toString();
//						}
//						else
//						{
//							throw new Exception("Operador ".concat(desc.getString(Constant.type))
//									.concat(" desconhecido para ").concat(auxClassUID).concat(".").concat(attributeObjectName));
//						}
//					}
//					else if (isHierarchyTop && attributeObjectName.trim().equals(Constant.id)) 
//					{
//						/*
//						 * Do nothing!
//						 */
//					}
//					else 
//					{
//						value = Type.getInstance().getValueFormattedByType(object.get(attributeObjectName), 
//								attributeSpec.getString(Constant.type));
//						if (value == null) 
//						{
//							operator.append(" IS ");
//						} 
//						else if (value.contains("%") && attributeSpec.getString(Constant.type).equals(Type.YC.String))
//						{
//							operator.append(" ILIKE ");
//						}
//						else
//						{
//							operator.append(" = ");
//						}
//					}
//					
//					if (isHierarchyTop && attributeObjectName.trim().equals(Constant.id)) 
//					{
//						
//					} 
//					else 
//					{
//						final String _attributeName = lcAttributeSpecName;
//						final String _auxClassUID = auxClassUID.toLowerCase();
//						final String _associationClassName = associationsClazzes.get(_auxClassUID).toString();
//						
//						filters.append(_auxClassUID).append("_").append(_associationClassName)
//								.append(".")
//								.append(_attributeName).append(operator.toString()).append(value);
//						
//						if (flag == 1 || flag == 2) 
//						{
//							filters.append(" ").append(connective).append(" ");
//							
//							filters.append(_auxClassUID)
//							.append("_").append(_associationClassName)
//									.append(".")
//									.append(_attributeName).append(flag == 1 ? " < " : " <= ").append(supValue)
//											.append(" ").append(criterion.getString(Constant.connective).trim().toUpperCase())
//											.append(" ");
//						}
//						else
//						{
//							filters.append(" ").append(criterion.getString(Constant.connective).trim().toUpperCase())
//									.append(" ");
//						}
//						
//						if (attributeSpec.getBoolean(Constant.isUnique))
//						{
//							break;
//						}
//					}
//				}
//			}
//		}
//		
//		if (objectSpec.has(Constant._classDef) 
//				&& objectSpec.getJSONObject(Constant._classDef).has(Constant._extends))
//		{
//			object.put(Constant.classUID, 
//					objectSpec.getJSONObject(Constant._classDef).getString(Constant._extends));
//			this.configQuery(object, null, classUID, criterion, filters, hierarchyLevel, associationMode, 
//					associationsClazzes, model);
//		}
//		
//		object.put(Constant.classUID, classUID);
//	}
}

