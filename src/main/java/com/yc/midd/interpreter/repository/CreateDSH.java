package com.yc.midd.interpreter.repository;

import org.json.JSONArray;
import org.json.JSONObject;

public interface CreateDSH
{
	public JSONArray save(JSONObject jsonObject) throws Exception;
	
	public JSONObject saveOne(JSONObject jsonObject) throws Exception;
}
