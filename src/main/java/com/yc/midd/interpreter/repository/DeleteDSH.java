package com.yc.midd.interpreter.repository;

public interface DeleteDSH
{
	public String deleteOne(String classUID, Long id) throws Exception;
}
