package com.yc.midd.interpreter;

import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.sql.DataSource;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.env.Environment;

import com.yc.midd.interpreter.repository.CreateDSH;
import com.yc.midd.interpreter.repository.ReadDSH;
import com.yc.midd.interpreter.repository.UpdateDSH;
import com.yc.utils.FileUtils;
import com.yc.utils.YCLogger;

@SpringBootApplication
public class InterpreterMicroService implements CommandLineRunner
{
	@Resource
	public CreateDSH createDSH;
	
	@Resource
	public ReadDSH readDSH;
	
	@Resource
	public UpdateDSH updateDSH;
	
	@Autowired
	private DataSource dataSource;
	
	@Autowired
	private Environment environment;
	
	public static void main(String... args) throws Exception
	{
		new SpringApplication(InterpreterMicroService.class).run(args);
	}
	
	@Override
	public void run(String... args) throws Exception
	{
		try
		{
			CEnv.getInstance(this.dataSource, 
					Boolean.valueOf(this.environment.getProperty("midd.sql_show")),
					Boolean.valueOf(this.environment.getProperty("midd.sql_drop_tables")), 
					Boolean.valueOf(this.environment.getProperty("midd.logging")));
			
			Boolean onBoot = 
					Boolean.valueOf(this.environment.getProperty("midd.onboot"));
			
			YCLogger.log("build statements on ".concat(onBoot ? "startup" : "the fly"));
			
			if (onBoot) 
			{
				CEnv.getInstance().buildSQLStatements(new JSONObject(
						new FileUtils().getDataFileFromClassLoader(
								this.environment.getProperty("midd.model.filename"))));
				
				YCLogger.log("SQL Statemens installed ... Ok");
			}
			
			YCLogger.log("LEnv installed ... Ok");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			
			System.exit(-1);
		}
	}
	
	@PreDestroy
	public void onExit()
	{
		
	}
}