package com.yc.midd.interpreter;

import java.io.Serializable;
import java.sql.Connection;

import javax.sql.DataSource;

import org.json.JSONException;
import org.json.JSONObject;

import com.yc.midd.interpreter.bsttm.SQLStatements;

@SuppressWarnings ("serial")
public class CEnv implements Serializable
{
	private static CEnv singleton;
	
	public final static Boolean LOG_USER = true;
	public final static Boolean LOG_GROUP = true;
	public final static Boolean LOG_DATE = true;
	
	final private DataSource dataSource;
	
	static private Boolean showSQL = false;
	
	static private Boolean dropTables  = false;
	
	static private Boolean logging  = false;
	
	static private JSONObject dataModel = null;
	
	private CEnv (DataSource dataSource, Boolean showSQL, Boolean dropTables, 
			Boolean logging) throws Exception
	{
		this.dataSource = dataSource;
		
		CEnv.showSQL = showSQL;
		
		CEnv.dropTables = dropTables;
		
		CEnv.logging = logging ;
	}
	
	public static synchronized CEnv getInstance () throws JSONException
	{
		return singleton;
	}
	
	public static synchronized CEnv getInstance (DataSource dataSource, 
			Boolean showSQL,  
			Boolean dropTables, 
			Boolean logging) throws Exception
	{
		if (singleton == null)
		{
			singleton = new CEnv(dataSource, showSQL, dropTables, logging);
		}
		
		return singleton;
	}
	
	static public Boolean isShowSQL()
	{
		return showSQL;
	}
	
	public static Boolean isDropTables()
	{
		return dropTables;
	}
	
	static public Boolean isLogging()
	{
		return logging;
	}
	
	public void buildSQLStatements(JSONObject dataModel) throws Exception
	{
		Connection connection = null;
		
		try
		{
			final SQLStatements sqlStatements = SQLStatements.getInstance();
			CEnv.dataModel = sqlStatements.reconfigureModel(dataModel);
			sqlStatements.build(CEnv.dataModel);
			
//			CEnv.log(CEnv.dataModel.toString(3));
			connection = this.dataSource.getConnection();
			sqlStatements.updateSchema(connection);
			
//			CEnv.log(SQLStatements.getStatements().toString(2));
		}
		catch (Exception e) 
		{
			throw e;
		}
		finally
		{
			if (connection != null) 
			{
				connection.close();
			}
		}
		
//		System.exit(0);
	}
	
	public DataSource getDataSource()
	{
		return this.dataSource;
	}
	
	static public JSONObject getDataModel ()
	{
		return CEnv.dataModel;
	}
}