package com.yc.midd.interpreter.controller;

import javax.annotation.Resource;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.yc.midd.interpreter.exception.MIDDControlledException;
import com.yc.midd.interpreter.service.PersistenceService;

@RequestMapping("/")
@RestController
public class DeleteController
{
	@Resource
	private PersistenceService service;
	
	@DeleteMapping(value = "/{classUID}/{id}")
	public ResponseEntity<String> deleteOne(
			@PathVariable("classUID") String classUID, @PathVariable("id") long id)
	{
		try
		{
			service.deleteOne(classUID, id);
			
			return ResponseEntity.ok("");
		}
		catch (MIDDControlledException e) 
		{
			return ResponseEntity.ok(e.getMessage().concat("\n"));
		}
		catch (Exception e)
		{
			return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED)
					.body(e.getMessage().concat("\n"));
		}
	}
}
