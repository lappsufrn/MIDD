package com.yc.midd.interpreter.controller;

import javax.annotation.Resource;

import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.yc.midd.interpreter.exception.MIDDControlledException;
import com.yc.midd.interpreter.service.PersistenceService;

@RequestMapping("/")
@RestController
public class CreateController
{
	@Resource
	private PersistenceService service;
	
	@PostMapping(value = "/", 
			consumes = MediaType.APPLICATION_JSON_VALUE, 
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> createOne(@RequestBody(required = true) String body)
	{
		try
		{
			return ResponseEntity.status(HttpStatus.CREATED).body(
					service.createOne(new JSONObject(body)).toString());
		}
		catch (MIDDControlledException e) 
		{
			return ResponseEntity.status(HttpStatus.CREATED).body(e.getMessage().concat("\n"));
		}
		catch (Exception e)
		{
			return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED)
					.body(e.getMessage().concat("\n"));
		}
	}
}
