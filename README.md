# MIDD - An initial implementation
This is a proof-of-concept of the *Metadata Interpretation Driven Development* (MIDD) methodology. For more information read the article (MIDD.pdf) posted in this repository.

The objective of this project is to demonstrate the operation of a software construction methodology (MIDD) that allows the complete reuse of your code to meet the demands of persistence (we could do this with others software concerns, like security) for applications from the most different domains of information systems without any change in code, despite changing data requirements. 

All that is required is the writing of a data model (see session: _usage (0)_) in json format to be loaded at the time of the project's initialization and, in addition, the correct use of HTTP endpoints described as following.

# Audience
Scientists and Engineers Software and Researchers, Software Developers, Students, etc.

# Invite
We invite everyone to collaborate with this project!
Send an e-mail to: juliogustavocosta@gmail.com

# Brief Description of Use

This implementation was built using Eclipse/Maven, Java/Spring-boot and Postgresql. To use it you need know and install this tools (use JRE version 11). 

#### Important:
As this implementation is a proof-of-concept, it has some limitations of synchronization between the representation made in json inserted in dmodel.json (file in the resources folder of the java project) and the representation that the interpreter automatically creates in the database. After the first initialization of the interpreter it creates a representation of the data model described by dmodel.json in the database. From that moment on, some types of changes made to dmodel.json will need to be made manually in the database. Changes such as: removal of attributes or classes, are cases that will require interference from the human operator of the interpreter to synchronize representations (between the dmodel.json file and the database). 

#### Environment Configure and Install

1. You need to install JRE 11 (or later) PostgreSQL 9.6 (or later) 

2. PostgreSQL config (pg_hba.conf):  the _SGDB authentication mode_ is __trust__; port: 5432 (default) 

3. Interpreter config: **Before run jar project compiled by maven**, you need create user and a database with its names defined in _application.properties_ (file in java project resources folder). If you want change this names in _application.properties_ do it. By default, to Library model, you will set postgresql database name as ds\_db and user admin\_ds\_db.

4. Model (**dmodel.json file**): the following models are to be described in the dmodel.json file. Each model must be run in different interpreter instances. Or if in the same instance, one at a time. The dmodel.json file is in the java project resources folder.

5. Service initialize: go to the project folder and run  command: **java -jar midd-interpreter-0.0.1-SNAPSHOT.jar&**

#### Model to Library Example 
(see [UML Clas Diagram](https://bitbucket.org/juliogustavocosta/library-oop-jpa/raw/5cec33a199e94811c8a5944018db12745be69f9d/umlclassdiagram.png))

```
{
    "GoodAndService": {
        "_classDef": {
        },
        "name": {
            "type": "String",
            "isUnique": true,
            "isNullable": false,
            "length": 12
        }
    },
    "Book": {
        "_classDef": {
            "_extends": "GoodAndService",
            "_specializationStrategy": "Join"
        },
        "isbn": {
            "type": "String",
            "isUnique": true,
            "isNullable": true,
            "length": 32
        }
    },
    "Renter": {
        "_classDef": {
        },
        "name": {
            "type": "String",
            "length": 128,
            "isUnique": false,
            "isNullable": false
        },
        "register": {
            "type": "String",
            "length": 64,
            "isUnique": true,
            "isNullable": false
        }
    },
    "Student": {
        "_classDef": {
            "_extends": "Renter",
            "_specializationStrategy": "Join"
        },
        "birthdate": {
            "type": "Date",
            "isUnique": false,
            "isNullable": false
        }
    },
    "Rental": {
        "_classDef": {
            "_uniqueTuple":  [ "student", "book", "start" ]
        },
        "student": {
            "type": "Student",
            "isUnique": false,
            "isNullable": false
        },
        "book": {
            "type": "Book",
            "isUnique": false,
            "isNullable": false
        },
        "start": {
            "type": "Date",
            "isUnique": false,
            "isNullable": false
        },
        "finish": {
            "type": "Date",
            "isUnique": false,
            "isNullable": true
        }
    }
}
```

#### Model to RentalCar Example:
(see [UML Clas Diagram](https://bitbucket.org/juliogustavocosta/rentalcar-oop-jpa/raw/5030c01dd4c466c155288d2fb7e43b0fe22aba74/umlclassdiagram.png))

```
{
    "GoodsAndServices": {
        "_classDef": {
        },
        "name": {
            "type": "String",
            "isUnique": true,
            "isNullable": false,
            "length": 64
        }
    },
    "Vehicle": {
        "_classDef": {
            "extends": "GoodsAndServices"
        },
        "brand": {
            "type": "String",
            "isUnique": false,
            "isNullable": false,
            "length": 32
        }
    },
    "Renter": {
        "_classDef": {
        },
        "name": {
            "type": "String",
            "length": 64,
            "isUnique": false,
            "isNullable": false
        },
        "register": {
            "type": "String",
            "length": 32,
            "isUnique": true,
            "isNullable": false
        }
    },
    "Client": {
        "_classDef": {
            "extends": "Renter"
        },
        "fiscalDoc": {
            "type": "String",
            "length": 32,
            "isUnique": true,
            "isNullable": false
        }
    },
    "Rental": {
        "_classDef": {
            "uniqueTuple":  [ "vehicle", "client", "start" ]
        },
        "vehicle": {
            "type": "Vehicle",
            "isUnique": false,
            "isNullable": false
        },
        "client": {
            "type": "Student",
            "isUnique": false,
            "isNullable": false
        },
        "start": {
            "type": "Date",
            "isUnique": false,
            "isNullable": true
        },
        "finish": {
            "type": "Date",
            "isUnique": false,
            "isNullable": false
        }
    }

}
```

*Obs*.: (_1_) when you run the jar, after compiling the java source code with maven, you will have an HTTP API for data persistence through the endpoints described below; (_2_) it is not possible to directly manipulate (create, read, update or delete) through the interpreter's endpoints classes that are generalizations to other classes

### (1) Create Data
##### (1.1) HTTP Message: POST
##### (1.2) URL (Request): http://localhost:8585/MIDD/Create
##### (1.3) Body (Request): 
```
[{
	'object': { 
		'classUID': 'Book', 
		'name': 'The Brothers Karamazov',
		'isbn': '1461.535A-234.1231N'
	} 
}]
```
```
curl command: curl --header "Content-Type: application/json" --request POST \
	--data '[{ "object": { "classUID":"Book", "name": "The Brothers Karamazov", "isbn":"1461.535A-234.1231N" } }]' \
	http://localhost:8585/MIDD/Create
```
##### (1.4) Status Code (Response): 201 (if success)
##### (1.5) Body (Response): 
```
[{ 
	'object': { 
		'classUID': 'Book', 
		'id': 1,
		'name': 'The Brothers Karamazov',
		'isbn': '1461.535A-234.1231N',
		'_version': 0
	} 
}]
```

### (2) Read Data
##### (2.1) HTTP Message: POST
##### (2.2) URL (Request): https://localhost:8585/MIDD/Read
##### (2.3) Body (Request): 
##### Filter Template
```
{
  'object': {
    'classUID': '', // Data class name
   ['attribute': 'value', \*]
  },
  'criterion': { 
    'toCount': false, // run a select to count registers
    'order': "asc", // for sorting mode
    'orderBy': "id", // attribute used for sorting
    'maxRegisters': 10, // number of register by page
    'firstRegister': 0 , // page number
    'connective': 'AND' // 'OR'
  },
  'associations': {
    'mode': true, // search through the associations tree
    'level': 1 // level of association to achieve in tree associations model
  }
}
```
```
curl command: curl --header "Content-Type: application/json" --request POST \
	--data '{ "object": { "classUID":"Book", "name": "The Brothers Karamazov" }, "criterion": { "toCount": false, "order": "asc", "orderBy": "id", "firstRegister": 0, "maxRegisters": 10, "connective": "AND" }, "associations": { "mode": false, "level": 0 } }' \
	http://localhost:8585/MIDD/Read
```

##### The following are some rules and possibilities for using the filter template for searching records

###### The attributes of the objects must be valued according to the following example:
```
'object': {
  'classUID': 'Book',
  'name': 'The Brothers Karamazov'
}

// this search returns tuples that 'nome' is equal to 'The Brothers Karamazov'.
```

###### For like operator usage put add '%' to search expression. '%lceu', for example
```
'object': {
  'classUID': 'Book',
  'nome': '%hers Karamazov'
}

// this search returns tuples whose name attribute values end with 'hers Karamazov'
```

###### other usage:
```
'object': {
  'classUID': 'Book',
  'nome': null
}

// this search returns tuples that 'nome' attribute in collection 'Book' no has value```
```

###### and other:
```
'object': {
  'classUID': 'Book', 
  'name': {
    'type': 'in',
    'in': [ 'Brothers', 'Idiot' ]
  }
}

//this search returns tuples that values 'Brothers' or 'Idiot' can be associate to 'name' attribute in collection 'Book'
```

###### and other:
```
'object': {
  'classUID': 'Student', 
  'nascimento': {
    type: 'range',
    'range': {
      sup: timestamp, // up limit
      inf: timestamp, // dowm limit
      include: Boolean, // true to operator be '<=' or '>='
      closed: Boolean, // if search is to be done between limits (sup and inf) or no
      connective: // 'OR' or 'AND'
    }
  }
}

// this is an example of setting to search records between dates, to attribute birthdate
```
##### (2.4) Status Code (Response): 200 (if success)
##### (2.5) Body (Response) (an example): 
```
{
	'data': [{  
		'classUID': 'Book', 
		'id': 1,
		'name': 'The Brothers Karamazov',
		'isbn': '1461.535A-234.1231N',
		'_version': 0
	}]
}
```

### (3) Update Data
##### (3.1) HTTP Message: POST
##### (3.2) URL (Request): http://localhost:8585/MIDD/Update
##### (3.3) Body (Request): 
```
[{ 
	'object': { 
		'classUID': 'Book', 
		'id': 1,
		'name': 'The Brothers Karamazov',
		'isbn': '0000.111B-222.3333K',
		'_version': 0
	},
	'updateTree': false
}]
```
```
curl command: curl --header "Content-Type: application/json" --request POST \
	--data '[{ "object": { "classUID": "Book", "id": 1, "name": "The Brothers Karamazov", "isbn": "0000.111B-222.3333K", "_version": 0 }, "updateTree": false }]' \
	http://localhost:8585/MIDD/Update
```
##### (3.4) Status Code (Response): 200 (if success)
##### (3.5) Body (Response): 
```
[{ 
	'object': { 
		'classUID': 'Book', 
		'id': 1,
		'name': 'The Brothers Karamazov',
		'isbn': '0000.111B-222.3333K',
		'_version': 1
	} 
}]
```

### (4) Delete Data
##### (4.1) HTTP Message: POST
##### (4.2) URL (Request): https://localhost:8585/MIDD/Delete
##### (4.3) Body (Request): 
```
[{ 
	'object': { 
		'classUID': 'Book', 
		'id': 1, 
		'_version': 1 
	} 
}]

curl command: curl --header "Content-Type: application/json" --request POST \
	--data '[{ "object": { "classUID":"Book", "id": 1, "_version": 1 } }]' \
	http://localhost:8585/MIDD/Delete```
```
##### (4.4) Status Code (Response): 200 (if success)
##### (4.5) Body (Response): 
```[]```
